package net.bencarson.playground.test.katas;

import net.bencarson.playground.katas.SquareDigit;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SquareDigitTest {
    @Test
    public void test() {
    	int myNumber = 9119;
        Assert.assertEquals(9119^2, new SquareDigit().squareDigits(9119));
    }
}