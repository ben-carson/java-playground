package net.bencarson.playground.test.jmockit;

import junit.framework.Assert;
import mockit.Mocked;
import net.bencarson.playground.exceptions.CageTooSmallException;
import net.bencarson.playground.objects.Ferret;
import net.bencarson.playground.objects.FerretCage;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bencarson on 9/17/15.
 */
public class SimpleJMockitTest {

    @Mocked
    public Ferret frt;

    List<Ferret> ferretList;
    public FerretCage cage;

    @Before
    public void setUp() {
        cage = new FerretCage();
        ferretList = new ArrayList<Ferret>();
        ferretList.add(frt);
        ferretList.add(frt);
    }

    @Test
    public void cageTest() {
        try {
            cage.loadTheCage(ferretList);
            Assert.assertEquals(ferretList.size(),cage.getTheCage().size());
        } catch (CageTooSmallException e) {
            Assert.fail("There should be enough room in the cage for the ferrets");
        }
    }

    @Test
    public void tooManyFerretsTest() {
        for(int a = 2; a < 6; a++) {
            ferretList.add(frt);
        }
        //Ensure there are more ferrets than there is room in the cage
        Assert.assertTrue(ferretList.size() > cage.getAnimalLimit());
        try {
            cage.loadTheCage(ferretList);
        } catch(CageTooSmallException ctse) {
            System.out.println(ctse.getMessage());
            Assert.assertNotNull(ctse.getMessage());
        }
    }

}
