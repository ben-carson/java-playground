package net.bencarson.playground.test.jmockit;

import net.bencarson.playground.enums.ElementTypeEnum;
import net.bencarson.playground.hashmapped.JookyObject;
import net.bencarson.playground.objects.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

/**
 * @author ben-carson
 * 3/18/16.
 * @implNote In order to accurately replicate the situation I'm in, I need to create a container of Jookie Objects, then,
 * have those objects copied to another container,then the two container's compared with the overridden hashCode
 * and equals methods
 */
public class JookyObjectTest {

    private final Map<ElementTypeEnum, List<JookyObject>> mapToLoad = new LinkedHashMap<>();
    private final Map<ElementTypeEnum, List<JookyObject>> mapToCheck = new LinkedHashMap<>();

    @Before
    public void setUp() {

        //create a series of JookyObjects for each Element Type
        for (ElementTypeEnum elementType : ElementTypeEnum.values()) {
            //build the list of JookyObjects to be stored on the final Map that will be checked against the .equals method
            List<JookyObject> listOfJookies = new ArrayList<>();

            //********** Jooky Object 1 *************//
            Person p1 = new Person();
            p1.setAge("36");
            p1.setName("Ben");

            Animal a1 = new Cat();
            a1.setName("Jake");
            a1.setFamily("Felis Catus");
            a1.setFormOfLocomotion("Strut");

            Ferret f1 = new Ferret();
            f1.setName("Scooter");
            f1.setFamily("Mustela putorius furo");
            f1.setFormOfLocomotion("scamper");

            //just use the defaults
            Animal a2 = new Cat();

            Set<Animal> animals = new HashSet<>();
            animals.add(a1);
            animals.add(a2);
            animals.add(f1);

            JookyObject testObject = new JookyObject();
            testObject.setFavoriteShow("Key & Peele");
            testObject.setFavoriteDrink("Tequila w/ twist of lime");
            testObject.setFavoritePerson(p1);

            testObject.setFavoriteAnimals(animals);

            listOfJookies.add(testObject);

            //********** Jooky Object 2 *************//
            JookyObject testObject2 = new JookyObject(
                    "favorite SHow",
                    "favorite Drink",
                    new Person("Mr. Wonderful"),
                    new HashSet<>()
            );

            listOfJookies.add(testObject2);

            //********** Jooky Object 3 *************//
            JookyObject testObject3 = new JookyObject(
                    "Daredevil",
                    "Sparkling Water",
                    new Person("Flanagan"),
                    new HashSet<>(
                            Arrays.asList(new Dog("Flanagan"),new Dog("Dottie"),new Cat("Ezio"))
                    )
            );

            listOfJookies.add(testObject3);

            //********** Jooky Object 4 *************//
            //Copy object so that the equals method will find a match
            JookyObject object2Copy = testObject2;

            listOfJookies.add(object2Copy);

            //Place the enum, list key/value pair into the LinkedHashMap
            mapToLoad.put(elementType, listOfJookies);

        }

    }

    @After
    public void destroy() {
        //close up shop
    }

    @Test
    public void testHashCode() {

        //copy-and-pasted object from above,
        //********** Jooky Object 2 *************//
        JookyObject testObject2 = new JookyObject(
                "favorite SHow",
                "favorite Drink",
                new Person("Mr. Wonderful"),
                new HashSet<>()
        );

        for(Map.Entry<ElementTypeEnum, List<JookyObject>> jookyEntry : mapToLoad.entrySet()) {
//            if(jookyEntry.) {

//            }
        }

        Assert.assertTrue(mapToLoad.containsValue(testObject2));

    }


}
