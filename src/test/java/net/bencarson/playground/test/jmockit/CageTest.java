package net.bencarson.playground.test.jmockit;

import mockit.Mocked;
import net.bencarson.playground.objects.Cage;
import net.bencarson.playground.objects.Cat;
import net.bencarson.playground.objects.Ferret;
import net.bencarson.playground.objects.FerretCage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by bencarson on 9/17/15.
 */
public class CageTest {

    Cage testCage;
    @Mocked
    Ferret f1;
    @Mocked
    Ferret f2;
    @Mocked
    Ferret f3;
    @Mocked
    Ferret f4;
    @Mocked
    Cat c1;
    @Mocked
    Cat c2;

    @Before
    public void setUp() {
        f1 = new Ferret();
        f1.setFamily("Carson");

        f2 = new Ferret();
        f2.setFamily("Sinakhonerath");

        f3 = new Ferret();
        f3.setFamily("Morgan");

        f4 = new Ferret();
        f4.setFamily("Beatrice");
    }

    @Test
    public void ferretCageTest() {

        FerretCage ferretCage = new FerretCage();
        Assert.assertNotNull(ferretCage);
        Assert.assertTrue(ferretCage.getTheCage().size() <= ferretCage.getAnimalLimit());

    }
}
