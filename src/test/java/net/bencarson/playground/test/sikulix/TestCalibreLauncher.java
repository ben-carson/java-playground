package net.bencarson.playground.test.sikulix;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import net.bencarson.playground.sikulix.CalibreLauncher;

public class TestCalibreLauncher {

	private CalibreLauncher testLauncher;
	
	@Before
	public void SetupTests() {
		testLauncher = new CalibreLauncher();
	}
	
	@Test
	public void testLaunchCalibre() {
		boolean didTestPass = false;
		try {
			didTestPass = testLauncher.launchCalibre();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		Assert.assertTrue(didTestPass);
	}
	
	@Test
	public void testCloseCalibre() {
		try {
			testLauncher.launchCalibre();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		Assert.assertEquals(0, testLauncher.closeCalibre());
	}

}
