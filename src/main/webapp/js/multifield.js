/**
 * experimental javascript to support ACR 305:
 * auto selection of radio buttons when the user selects a range
 * from the range of motion inputs
 */
$(document).ready(function() {

	clearAndDisableDelucaFields();
    
	var pFlexActual = $("#Facts_PlantarFlexionInitial");
	var pFlexDeluca = $("#Facts_PlantarFlexionDeluca");
	var pFlexMitch = $("#Facts_PlantarFlexionMitchell")
	var pFlexFlare = $("#Facts_PlantarFlexionFlareUp");
	var romRadioBtn = $("input:radio[name=Facts_Range_Of_Motion]");
	//I may want to do this onblur instead, for greater efficiency
	pFlexActual.on("keyup", function(){
		var pFlexActualVal = pFlexActual.val();
		if(pFlexActualVal != null && ( isNaN(pFlexActualVal) || pFlexActualVal < 0)) {
			alert("Only positive numeric values allowed.");
			pFlexActualVal.val("0");	//reset to zero
			pFlexActualVal.focus();		//set focus to the problem input
		} else if(pFlexActualVal > 0 && pFlexActualVal < 10) {
			romRadioBtn[0].checked = true;
		} else if(pFlexActualVal >= 10 && pFlexActualVal < 29) {
			romRadioBtn[1].checked = true;
		} else if(pFlexActualVal >= 30 && pFlexActualVal < 45) {
			romRadioBtn[2].checked = true;
		} else if(pFlexActualVal >= 45) {
			romRadioBtn[3].checked = true;
		}
	});
});

function clearAndDisableDelucaFields() {
	$("#Facts_PlantarFlexionDeluca")
        .val("")
		.prop("disabled",true);
	$("#Facts_PlantarFlexionMitchell")
        .val("")
        .prop("disabled",true);
    $("#Facts_PlantarFlexionFlareUp")
        .val("")
        .prop("disabled",true);
}

function activateDelucaFields() {
    alert("greet");
	$("#Facts_PlantarFlexionDeluca").prop("disabled",false);
	$("#Facts_PlantarFlexionMitchell").prop("disabled",false);
    $("#Facts_PlantarFlexionFlareUp").prop("disabled",false);
}