package com.dojang.scjp;

public class CharLiteral {
	public static void main(String args[]) {
		char character;
		character = 74;			//decimal
		System.out.println("character 72 is "+character);
		character = 'J';		//literal
		System.out.println("character 'J' is "+character);
		character = 0x4A;		//hexadecimal
		System.out.println("character 0x4A is "+character);
		character = 0112;		//octal
		System.out.println("character 0112 is "+character);
		character = '\u004A';	//unicode, note the escape character
		System.out.println("character '\\u004A' is "+character);
	}
}
