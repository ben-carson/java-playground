package com.dojang.scjp;

public class VariableScope {

	int x;
	double dubVal = 3.14;
	public void testMethod(short methodLocalScope) {
//		k = 12;// can only be instantiated AFTER its declared. Declaration must come
		//first on a preceding line of code, but within the appropriate code block
		for(int k = 0; k<12; ++k) {
			System.out.print(k);
			{
				char a = 'c';
				String temp = "" + k + a;
			}
		}
		int k;
		//capitalization of float or long does not matter
		float f1 = 29f;
		float f2 = 24F;
		long l1 = 12l;
		Long l2 = 12L;
	}
}
