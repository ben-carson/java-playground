package com.dojang.scjp;

import java.util.TreeSet;

public class CollectionPlay {

	public static void main(String[] args) {
		new CollectionPlay();
	}
	CollectionPlay() {
		TreeSet<String> treeSet = new TreeSet<String>();
		/*note the order that these are printed:
		 * lexicographic order, aka ASCII order
		 */
		treeSet.add("zero");
		treeSet.add("Zero");
		treeSet.add("One");
		treeSet.add("two");
		treeSet.add("three");
		treeSet.add("4");
		for(String str:treeSet) {
			System.out.println("element: "+str);
		}
	}

}
