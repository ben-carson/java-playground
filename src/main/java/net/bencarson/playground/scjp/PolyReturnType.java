package com.dojang.scjp;

import java.util.ArrayList;
import java.util.List;

public class PolyReturnType {

	class James { }
	class Ben extends James {
		List<Ben> returnBens() {
			List<Ben> bens = new ArrayList<Ben>();
			return bens;
		}
		
		List<James> returnJameses() {
			List<James> jameses = new ArrayList<James>();
			return jameses;
		}
	}
	public static void main(String... sup) {
		PolyReturnType p = new PolyReturnType();
		//TODO: WTF is this?
		Ben b = p.new Ben();
		b.returnBens();
	}
}
