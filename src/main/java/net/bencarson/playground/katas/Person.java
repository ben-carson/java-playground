package net.bencarson.playground.katas;

/**
 * @author bencarson
 *         created on 4/8/16.
 */
public class Person {
    String name;

    public Person(String personName) {
        name = personName;
    }

    public String greet(String yourName) {
        return String.format("Hi %s, my name is %s", yourName, name);
    }
}
