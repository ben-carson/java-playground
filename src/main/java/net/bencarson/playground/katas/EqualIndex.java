package net.bencarson.playground.katas;

/**
 * @author bencarson
 *         created on 4/15/16.
 */
public class EqualIndex {

    public static int findEvenIndex(int[] arr) {
        int sumOfRight = 0;
        int sumOfLeft = 0;
        int indexOfEquality = -1;

        for (int a = 0; a < arr.length; a++ ) {
            sumOfRight += arr[a];
        }
        //iterate through the input array, searching for the index of the element that has equal sums on both sides
        for ( int a = 1; a < arr.length; a++ ) {
            sumOfLeft += arr[a-1];
            sumOfRight -= arr[a+1];
            if( sumOfLeft == sumOfRight ) {
                indexOfEquality = a;
                return indexOfEquality;
            }
        }
        return indexOfEquality;
    }

}
