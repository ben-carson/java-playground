package net.bencarson.playground.katas;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SquareDigit {

    public int squareDigits(int n) {

        String intAsStr = Integer.toString(n);
        StringBuilder result = new StringBuilder();
        for(int a = 0; a<intAsStr.length(); a++) {
            String singleDigit = String.valueOf(intAsStr.charAt(a));
            result.append(Integer.parseInt(singleDigit)^2);
        }

        return Integer.parseInt(result.toString());

    }

    public static int findInt(int[] intArr) {
        int theOddNumber = -1;
        Set<Integer> alreadyUsedInts = new HashSet<Integer>();
        Arrays.sort(intArr);
        for(int a = 0; a < intArr.length; a++) {

        }
        return theOddNumber;
    }
}