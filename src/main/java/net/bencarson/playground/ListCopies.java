package net.bencarson.playground;

import net.bencarson.playground.objects.Person;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListCopies {

	
	public static void main(String[] args) {
		List<Person> listOfPersons = new ArrayList<Person>();
		List<Person> secondListOfPersons = new ArrayList<Person>();
		listOfPersons.add(new Person("Ben"));
		listOfPersons.add(new Person("Chuck"));
		listOfPersons.add(new Person("Kamal"));
		
		secondListOfPersons.addAll(listOfPersons);
		listOfPersons.removeAll(listOfPersons);
		Iterator<Person> listItr = listOfPersons.iterator();
		
		while(listItr.hasNext()) {
			System.out.println("'"+listItr.next().getName()+"'");
		}

		listItr = secondListOfPersons.iterator();
		while(listItr.hasNext()) {
			System.out.println(listItr.next().getName());
		}
		
	}

}
