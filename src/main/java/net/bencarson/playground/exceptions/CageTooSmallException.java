package net.bencarson.playground.exceptions;

/**
 * Created by bencarson on 9/18/15.
 */
public class CageTooSmallException extends Exception {

    public CageTooSmallException(String smallCageMessage) {
        super(smallCageMessage);
    }

}
