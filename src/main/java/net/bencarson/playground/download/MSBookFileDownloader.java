package net.bencarson.playground.download;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class MSBookFileDownloader {

	/**
	 * I've had this list of Microsoft books that can be downloaded. 
	 * No idea if they are useful or not, but I'll never know until I can 
	 * at least download them via their short URLs and read the titles.
	 * http://www.mssmallbiz.com/ericligman/Key_Shorts/MSFTFreeEbooks.txt
	 * @param args
	 */
	private static String theURL = "http://www.mssmallbiz.com/ericligman/Key_Shorts/MSFTFreeEbooks.txt";
	private static URL msFileList;
	
	private static void initDownloader() throws MalformedURLException {
		msFileList = new URL(theURL);
	}
	
	
	public static void main(String[] args) {
		try {
			initDownloader();
			ReadableByteChannel rbc = Channels.newChannel(msFileList.openStream());
			FileOutputStream fos = new FileOutputStream("ebookList.txt");
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		} catch (MalformedURLException e) {
			System.err.println("There is an error with the URL's format");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("there was an error opening the byte channel stream");
			e.printStackTrace();
		}

	}

}
