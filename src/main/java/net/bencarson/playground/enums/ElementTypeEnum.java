package net.bencarson.playground.enums;

/**
 * @author bencarson
 *         created on 3/22/16.
 */
public enum ElementTypeEnum {
    WATER("Water"),
    EARTH("Earth"),
    FIRE("Fire"),
    AIR("Air"),
    HEART("Heart");

    private String description;

    //argument constructor
    ElementTypeEnum(String desc) {
        this.description = desc;
    }

    public String getDescription() {
        if(this.description == null) {
            this.description = "";
        }
        return this.description;
    }
}
