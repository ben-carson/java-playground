package com.dojang.interfaces;

public class Cat implements Animal {

	@Override
	public int getAge() {
		return 3;
	}

	@Override
	public String getFurColor() {
		return furColor;
	}

	public static void main(String[] args) {
		Cat cat = new Cat();
		System.out.println("cats age:" + cat.getAge());
//		cat.furColor = "black";
		System.out.println("cats color:"+cat.getFurColor());
	}
	
}
