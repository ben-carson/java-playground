package com.dojang.interfaces;

public interface Animal {

	public String furColor = new String("white");
	int age = 0;
	
	public String getFurColor();
	
	public int getAge();
}
