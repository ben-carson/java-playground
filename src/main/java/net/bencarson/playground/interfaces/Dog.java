package com.dojang.interfaces;

public class Dog implements Animal {

	private static String name;
	protected String furColor;
	
	@Override
	public int getAge() {
		return age;
	}

	@Override
	public String getFurColor() {
		return furColor;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	public String getName() {
		return this.name;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Dog dog = new Dog();
		dog.name = "Fido";
		System.out.println(dog.name+"'s age:" + dog.getAge());
		//override the 'furcolor' of the interface
		dog.furColor = "black";
		System.out.println(dog.name+"'s fur color:"+dog.getFurColor());
		
		Dog[] a = {new Dog(),new Dog()};
		a[0].furColor = "green";
		a[1].furColor = Animal.furColor;
		for(Dog ani: a) {
			System.out.println(ani.furColor);
		}
		
	}

}
