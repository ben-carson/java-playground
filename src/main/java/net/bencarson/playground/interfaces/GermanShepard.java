package com.dojang.interfaces;

public class GermanShepard extends Dog {

	public static void main(String[] args) {
		GermanShepard specificBreed = new GermanShepard();
//		specificBreed.age = 4;
		specificBreed.setName("Doggie");
		System.out.println("shepard's name:"+specificBreed.getName());
		Dog dog = new Dog();
		//because 'name' is static in the Dog class, only one Dog
		//object is loaded in the JVM at runtime. This means that
		//anytime the variable is changed, it is changed for ALL
		//objects that inherit from this Dog parent.
		dog.setName("Fluffy");
		System.out.println("dog's name:"+dog.getName());
		System.out.println("shepard's name:"+specificBreed.getName());
		
	}

}
