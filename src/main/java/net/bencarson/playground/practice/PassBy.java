package net.bencarson.playground.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by bencarson on 2/17/16.
 */
public class PassBy {
    public static void main(String [] args) {
        List sourceList = Arrays.asList("Java","is","awesome","!");
        List passByValue = new ArrayList<>();
        List passByReference = passByValue;

        passByValue.addAll(sourceList);


        PassBy pb = new PassBy();
        pb.printList("pass by value", passByValue);

        passByValue.add("Jooky!");

        pb.printList("pass by reference",passByReference);

    }

    private void printList(String comment, List<String> strList) {
        System.out.println();
        System.out.println(comment);
        for(String str : strList) {
            System.out.println(str);
        }
        System.out.println();
    }


}
