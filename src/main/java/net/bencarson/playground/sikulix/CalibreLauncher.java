package net.bencarson.playground.sikulix;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.sikuli.script.App;

public class CalibreLauncher {
	//3Wjjksv6K_2fwFktLaUz
	private Logger _log = Logger.getLogger(CalibreLauncher.class.getName());
	private App calibreApp;
	
	public boolean launchCalibre() throws IOException {
		boolean isAppFound = false;
		calibreApp = new App("calibre");
		if(calibreApp.getPID()<0) {
			File calibreDir = new File("G:\\dev\\tools\\Calibre Portable");
			_log.info("calibre folder is " + calibreDir.getAbsolutePath());
			ProcessBuilder calibreBuilder = 
				new ProcessBuilder(calibreDir.getAbsolutePath()+"\\calibre-portable.exe");
			calibreBuilder.directory(calibreDir);
			Process calibre = calibreBuilder.start();
			//calibreApp = new App("calibre");
		}
		if(calibreApp.focus() != null) {
			isAppFound = true;
		}
		return isAppFound;
	}
	
	public int closeCalibre() {
		return App.close("calibre");
	}
	
}
