package net.bencarson.playground;

import net.bencarson.playground.objects.Person;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class SerialExperiments {

	/*
	 * I want to know if lists of objects, once converted to a Serializable object, can be
	 * converted back to their original form, with no loss of information.
	 */
	public static void main(String [] args) {
		ArrayList<Person> personList = new ArrayList<Person>();
		Person person1 = new Person("Ben");
		Person person2 = new Person("Chuck");
		Person person3 = new Person("Neeraja");
		
		personList.add(person1);
		personList.add(person2);
		personList.add(person3);
		
		//convert this list to a Serialized object
		try {
			//serialize object into a new file
			FileOutputStream fileStream = new FileOutputStream("serialized.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileStream);
			out.writeObject(personList);
			out.close();
			fileStream.close();
			
			System.out.println("Serialized data has been saved in serialized.ser");
			
			FileInputStream fileIn = new FileInputStream("serialized.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			ArrayList<Person> cerealPeople = (ArrayList<Person>) in.readObject();
			in.close();
			fileIn.close();
			
			printArray(cerealPeople);
			
		} catch (IOException e) {
			System.err.println("BOMB!");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Bleeerrrrp!");
			e.printStackTrace();
		}
		
	}
	
	private static void printArray(ArrayList<Person> personList) {
		Iterator<Person> itr = personList.iterator();
		while(itr.hasNext() ) {
			Person currentPerson = itr.next();
			System.out.println("Name: " + currentPerson.getName());
			System.out.println("Age: " + currentPerson.getAge());
			System.out.println("+------------------+");
		}
	}
	
}
