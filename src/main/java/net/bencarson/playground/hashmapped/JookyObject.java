package net.bencarson.playground.hashmapped;

import net.bencarson.playground.objects.Animal;
import net.bencarson.playground.objects.Person;

import java.util.HashSet;
import java.util.Set;

/**
 * @author ben-carson
 * 3/18/16
 */
public class JookyObject {

    private String favoriteShow;

    private String favoriteDrink;

    private Person favoritePerson;

    private Set<Animal> favoriteAnimals = new HashSet<>();

    public JookyObject() {
        favoriteShow = new String();
        favoriteDrink = new String();
        favoritePerson = new Person("default");
    }
    public JookyObject(String favShow, String favDrink, Person favPerson, Set<Animal> favAnimals) {
        this.favoriteShow = favShow;
        this.favoriteDrink = favDrink;
        this.favoritePerson = favPerson;
        this.favoriteAnimals.addAll(favAnimals);
    }

    public String getFavoriteShow() {
        return favoriteShow;
    }
    public void setFavoriteShow(String newFavShow) {
        this.favoriteShow = newFavShow;
    }

    public String getFavoriteDrink() {
        return favoriteDrink;
    }
    public void setFavoriteDrink(String favoriteDrink) {
        this.favoriteDrink = favoriteDrink;
    }


    public Person getFavoritePerson() {
        return favoritePerson;
    }
    public void setFavoritePerson(Person favoritePerson) {
        this.favoritePerson = favoritePerson;
    }

    public Set<Animal> getFavoriteAnimals() {
        return favoriteAnimals;
    }
    public void setFavoriteAnimals(Set<Animal> favoriteAnimals) {
        this.favoriteAnimals = favoriteAnimals;
    }

    @Override
    public boolean equals(Object o) {
        //if this and o are the exact same object (i.e. memory location)
        if (this == o) {
            return true;
        }
        //if object being compared to this object is null, there's no way they match
        if(o == null) {
            return false;
        }
        //if objects being compared are not the same class, there's no way they match
        if(this.getClass() != o.getClass()) {
            return false;
        }
        //compare the member variables of the two JookieObjects
        JookyObject jookieToCompare = (JookyObject)o;
        //first, check if both values are null (a valid match, meaning 'true' for equals) and then check if values DON'T match
        if(!(this.favoriteShow == null && jookieToCompare.favoriteShow == null) &&
                !this.favoriteShow.equals(jookieToCompare.favoriteShow)) {
            return false;
        }
        if( !(this.favoriteDrink == null && jookieToCompare.favoriteDrink == null) &&
                !this.favoriteDrink.equals(jookieToCompare.favoriteDrink)) {
            return false;
        }
        if( !(this.favoritePerson == null && jookieToCompare.favoritePerson == null) &&
                !this.favoritePerson.equals(jookieToCompare.favoritePerson)) {
            return false;
        }
        if( !(this.favoriteAnimals.isEmpty() && jookieToCompare.favoriteAnimals.isEmpty()) ) {
            return !this.favoriteAnimals.equals(jookieToCompare.favoriteAnimals);
        }
        //if execution has made it to this point, everything matches, so return true
        return true;
    }

    @Override
    public int hashCode() {
        int result = favoriteShow != null ? favoriteShow.hashCode() : 0;
        result = 31 * result + (favoriteDrink != null ? favoriteDrink.hashCode() : 0);
        result = 31 * result + (favoritePerson != null ? favoritePerson.hashCode() : 0);
        result = 31 * result + (!favoriteAnimals.isEmpty() ? favoriteAnimals.hashCode() : 0);
        return result;
    }
}
