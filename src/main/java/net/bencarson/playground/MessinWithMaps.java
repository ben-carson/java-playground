package net.bencarson.playground;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class MessinWithMaps {

	public static void main(String[]args) {
		Map<Integer, String> studyMap = new HashMap<Integer, String>();
		String testString = "This is my sample text";
		
		//load up the HashMap with sample data
		studyMap = MessinWithMaps.loadTheMapWithData(studyMap, testString);
		//display the map's current data
		printTheMap(studyMap);
		
		//Now, retrieve the 5th element, and then try to insert it right back into the same spot to see what happens
		Integer key = 5 * 10;
		String valueToTest = studyMap.get(key);
		studyMap.put(key, valueToTest);
		printTheMap(studyMap);
		
		//Now, what happens when I override the 5th element?
		studyMap.put(key, "Gobbledy-Gook!");
		printTheMap(studyMap);
	}
	
	private static HashMap<Integer, String> loadTheMapWithData(Map<Integer, String> theMap, String sampleText) {
		theMap = new HashMap<Integer, String>();
		for(int a = 0; a <= sampleText.length(); a++) {
			theMap.put(a*10, StringUtils.substring(sampleText, 0, a));
		}
		return (HashMap<Integer, String>) theMap;
	}
	
	private static void printTheMap(Map<Integer,String> theMapToPrint) {
		Iterator<Integer> keyItr = theMapToPrint.keySet().iterator();
		System.out.println("+------------------+");
		while(keyItr.hasNext()) {
			Integer currIndex = keyItr.next();
			System.out.print("["+currIndex.toString()+"]:");
			System.out.println(theMapToPrint.get(currIndex));
		}
		System.out.println("+------------------+");
	}
	
}
