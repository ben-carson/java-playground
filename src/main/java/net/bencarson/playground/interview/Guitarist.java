package net.bencarson.playground.interview;

/**
 * @author bencarson
 *         created on 4/18/16.
 */
public class Guitarist implements Musician {
    private Guitar myAxe = new Guitar();

    @Override
    public boolean hasInstrument() {
        return true;
    }

    //Accessor methods
    public Guitar getMyAxe() {
        if(myAxe == null) {
            myAxe = new Guitar();
        }
        return myAxe;
    }
    public void setMyAxe(Guitar newGuitar) {
        myAxe = newGuitar;
    }

}

