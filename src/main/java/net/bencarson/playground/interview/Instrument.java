package net.bencarson.playground.interview;

/**
 * @author bencarson
 *         created on 4/18/16.
 */
public abstract class Instrument {
    public abstract String makeSound();
}
