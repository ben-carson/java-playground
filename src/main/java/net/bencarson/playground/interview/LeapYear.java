package net.bencarson.playground.interview;

/**
Write a program to output "yes" if a given year is a leap year or "no" if it is
not. Leap years include any years that can be evenly divided by 4 (such as 2012,
 2016) except those that can be evenly divided by 100 (such as 2100, 2200) and
 those that can be evenly divided by 400, then it is (such as 2000, 2400).
*/

/**
 * @author ben-carson
 * @date March 28, 2016
 * @url https://www.interviewzen.com/interview/3kWG8Bq
 */
public class LeapYear {

  public static void main(String [] args) {
    try {
      //convert the input string into an integer as well as do some error checking
      Integer inputYear = LeapYear.convertInputToInteger(args[0]);
      // Evenly divide by 4 indicates a leap year
      if( inputYear % 4 == 0 ) {
        //...except when the leap year is cleanly divisble by 100
        if ( inputYear % 100 == 0 ) {
          //..however, if the year is ALSO cleanly divisible by 400, it IS a leap year
          if ( inputYear % 400 == 0 ) {
            System.out.println("yes");
          } else {
            //...divisible by 100, but not 400; so not a leap year
            System.out.println("no");
          }
        }
      }
    }
    catch (NumberFormatException nfe) {
      System.err.println("Error: Please provide a properly formatted year.");
    }
  }

  private static Integer convertInputToInteger(String str) throws NumberFormatException {
    Integer input = null;
    if( str.length() == 4 ) {
      input = Integer.parseInt(str);
    } else {
      throw new NumberFormatException("There are not 4 characters in the input provided.");
    }

    return input;
  }

}