package net.bencarson.playground.interview;

/**
 * @author bencarson
 *         created on 4/18/16.
 */
public class Vocalist implements Musician {

    @Override
    public boolean hasInstrument() {
        return false;
    }

}
