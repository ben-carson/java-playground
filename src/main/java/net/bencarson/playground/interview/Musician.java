package net.bencarson.playground.interview;

/**
 * @author bencarson
 *         created on 4/18/16.
 */
public interface Musician {
    public boolean hasInstrument();
}
