package net.bencarson.playground.interview;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bencarson
 *         created on 3/28/16.
 * @url https://www.interviewzen.com/interview/3kWG8Bq
 *
 * Implement a set of classes to model a band with the following objects: Musician, Guitar, Instrument, Guitarist, Vocalist.
 *
 * hierarchy:
 * -Guitar IS A -Instrument
 * -Guitarist IS A -Musician
 * -Vocalist IS A Musician
 * Guitarist HAS A Guitar
 * Band HAS Musicians
 */

public class Band {

    private List<Musician> bandMembers;

    public void addMember( Musician newMember ) {
        this.bandMembers.add(newMember);
    }

    //Accessor methods
    public List<Musician> getBandMembers() {
        if ( bandMembers == null ) {
            bandMembers = new ArrayList<>();
        }
        return bandMembers;
    }
    public void setBandMembers( List<Musician> newBand ) {
        this.bandMembers = newBand;
    }

}