package net.bencarson.playground.interview;

/**
 * @author bencarson
 *         created on 4/18/16.
 */
public class Guitar extends Instrument {

    @Override
    public String makeSound() {
        return "Wail";
    }

}
