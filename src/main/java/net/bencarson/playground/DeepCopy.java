package net.bencarson.playground;

import net.bencarson.playground.objects.Person;

public class DeepCopy {
	
	public static void main(String [] args) {
		Person p = new Person();
		p.setName("Ben");
		p.setAge("222");
		Person p1 = new Person();
		p1 = p;
		
		p1.setName("Kamal");
		System.out.println(p.getName());
		System.out.println(p1.getName());
	}
	
}
