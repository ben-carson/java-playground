package net.bencarson.playground.objects;

/**
 * @author ben-carson
 * 9/17/15.
 */
public class Ferret extends Animal {

    public Ferret() {
        setName("Maggie");
        setFamily("Animalia");
        setFormOfLocomotion("claw paws");
    }

    @Override
    public void speak() {
        System.out.println("gr gr gr grk grk");
    }
}
