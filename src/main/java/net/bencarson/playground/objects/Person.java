package net.bencarson.playground.objects;

import java.io.Serializable;

/**
 * @author ben-carson
 */
public class Person implements Serializable {

	private static final long serialVersionUID = 1231906229743466298L;
	private String name;
	private String age;

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Person() {
		super();
	}
	
	public Person(String personName) {
            name = personName;
    }

    public String greet(String yourName) {
            return String.format("Hi %s, my name is %s", name, yourName);
    }
	public static void main(String [] args) {
    	Person p = new Person("Ben");
    	System.out.println(p.greet("jooky"));
    }
}