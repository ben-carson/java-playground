package net.bencarson.playground.objects;

/**
 * @author bencarson
 *         created on 3/21/16.
 */
public class Dog extends Animal {

    /**
     * Simple constructor with Dog's name supplied
     * @param name
     */
    public Dog(String name) {
        super.setName(name);
    }

    /**
     * Simple constructor with Dog's name and mode of travel supplied
     * @param name
     * @param travelMode
     */
    public Dog(String name, String travelMode) {
        super.setName(name);
        super.setFormOfLocomotion(travelMode);
    }

    /**
     * overridden speak method.
     * Required implementation via Animal parent class
     */
    @Override
    public void speak() {
        System.out.println("Bark!");
    }
}
