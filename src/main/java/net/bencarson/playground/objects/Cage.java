package net.bencarson.playground.objects;

import net.bencarson.playground.exceptions.CageTooSmallException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ben-carson
 * 9/17/15.
 */
public abstract class Cage {

    List<Animal> theCage;

     int animalLimit = 0;

    public Cage() {
        animalLimit = 2;
        theCage = new ArrayList<Animal>(animalLimit);
    }

    /**
     * Unload all the animals that are currently in the cage.
     * Set the animal limit to '0', just so that it needs to be
     * updated by the developer when they try to add more animals.
     */
    public void emptyTheCage() {
        this.animalLimit = 0;
        this.theCage.clear();
    }

    public abstract List<? extends Animal> getTheCage();

    /**
     * This method loads animals into a cage
     * @param occupants
     * @param limit
     * @return
     */
    public abstract int loadTheCage(List<? extends Animal> occupants, int limit)
            throws CageTooSmallException;

    public abstract void setTheCage(List<? extends Animal> theCage);

    public int getAnimalLimit() {
        return animalLimit;
    }

    public void setAnimalLimit(int animalLimit) {
        this.animalLimit = animalLimit;
    }


}
