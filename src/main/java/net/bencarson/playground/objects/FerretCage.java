package net.bencarson.playground.objects;

import net.bencarson.playground.exceptions.CageTooSmallException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ben-carson
 * 9/18/15.
 */
public class FerretCage extends Cage {

    private int animalLimit = 4;
    private List<Ferret> theCage;

    public FerretCage() {
        this.animalLimit = 4;
        this.theCage = new ArrayList<Ferret>(this.animalLimit);
    }

    /**
     * The method puts ferrets into a cage
     * It either throws an exception if too many ferrets are added to the cage, or returns the number
     * of ferrets added to the cage.
     * @param ferrets
     * @return int representing number of ferrets loaded into cage
     * @throws CageTooSmallException
     */
    public int loadTheCage(List<Ferret> ferrets) throws CageTooSmallException {
        if (ferrets.size() > this.getAnimalLimit()) {
            throw new CageTooSmallException("You can't stuff that many ferrets in this cage!");
        } else {
            theCage = (List<Ferret>)ferrets;
        }
        return ferrets.size();
    }

    public List<Ferret> getTheCage() {
        if(theCage == null) {
            theCage = new ArrayList<Ferret>(animalLimit);
        }
        return theCage;
    }

    @Override
    public int loadTheCage(List<? extends Animal> occupants, int limit)
        throws CageTooSmallException {
        this.setAnimalLimit(limit);
        return loadTheCage((List<Ferret>) occupants);
    }

    @Override
    public void setTheCage(List<? extends Animal> theCage) {
        this.theCage = (List<Ferret>) theCage;
    }


}
