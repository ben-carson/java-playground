package net.bencarson.playground.objects;

/**
 * @author ben-carson
 * 9/17/15.
 */
public abstract class Animal {

    private String name;
    private String family;
    private String formOfLocomotion;

    public abstract void speak();

    //Accessor methods
    public String getName() {
        return name;
    }
    public void setName(String newName) {
        name = newName;
    }

    public String getFormOfLocomotion() {
        return formOfLocomotion;
    }

    public void setFormOfLocomotion(String formOfLocomotion) {
        this.formOfLocomotion = formOfLocomotion;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

}
