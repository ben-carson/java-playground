package net.bencarson.playground.objects;

/**
 * @author Ben-carson
 *  9/17/15.
 */
public class Cat extends Animal {

    /**
     * Default constructor
     *
     */
    public Cat() {
        setName("Adam");
        setFamily("Animalia");
        setFormOfLocomotion("Cute widdle weggies!");
    }

    /**
     * Constructor where a name is supplied.
     * @param name
     */
    public Cat(String name) {
        setName(name);
    }

    @Override
    public void speak() {
        System.out.println("Mrowr!");
    }
}
