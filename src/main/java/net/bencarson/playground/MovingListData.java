package net.bencarson.playground;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MovingListData {
	
	public static void main (String [] args) {
		//create list
		List<String> list1 = new ArrayList<String>();

		//populate list
		for(int a = 0; a <= 9; a++) {
			list1.add((a+1)+"");
		}
		
		//move data
		String testVariable = list1.get(5);
		System.out.println(testVariable);
		
		list1.removeAll(list1);
		list1.add(testVariable);
		Iterator<String> itr = list1.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

}
