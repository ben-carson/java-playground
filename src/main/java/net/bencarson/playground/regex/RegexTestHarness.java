package com.dojang.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTestHarness {

    public static void main(String[] args){
//    	pattern: 	.+?java.sql.SQLException
//    	source:		Error: PreparedStatementCallback; SQL [insert into XREF.ICARE_360_TX_LOG   (   LOG_ID,  DSS_CUST_ID,  DSS_ID,  TX_SYS_NAME,  EVENT_NAME,  ATTEMPT_SEQ,  RESULT_CODE,  RESULT_TEXT,  GENERAL_MESSAGE,  ERROR_MESSAGE,  CREATE_DATE,  CREATE_USER, CONTACT_OBJID) values  ( xref.sq_icare_360_tx_log_id.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?,  sysdate, ?, ? )]; ORA-01400: cannot insert NULL into (&quot;XREF&quot;.&quot;ICARE_360_TX_LOG&quot;.&quot;CREATE_USER&quot;)
//    	; nested exception is java.sql.SQLException: ORA-01400: cannot insert NULL into (&quot;XREF&quot;.&quot;ICARE_360_TX_LOG&quot;.&quot;CREATE_USER&quot;)

    	Pattern pattern = 
        	Pattern.compile(args[0].trim());

        Matcher matcher = 
        	pattern.matcher(args[1].trim());

        boolean found = false;
        while (matcher.find()) {
        	System.out.println("I found the text \""+matcher.group()+"\" starting at " +
               "index "+matcher.start()+" and ending at index "+matcher.end()+". ");
            found = true;
        }
        if(!found){
            System.out.println("No match found.");
        }
    }
}

