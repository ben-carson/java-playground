package com.dojang.statc;

public class Main {

	/*Output is:
	 *  B.a
	 *  A.b
	 *  B.a
	 *  B.b
	 *  
	 *  A type of variable 'a' is A ... but concrete object is of type B, so a call to
	 *  overridden method a() executes a method from concrete object of type B,	not a
	 *  method from class A.
	 *  In case of static method b() this works in a different way - the method b() from
	 *  class A is called (A is declared type of variable a).
	 */ 
    public static void main(String[] args) {
        A a = new B();
        a.a();
        a.b();

        B b = new B();
        b.a();
        b.b();
    }

}
