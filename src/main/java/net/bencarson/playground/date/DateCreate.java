package com.dojang.date;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateCreate {

	public String prependZeroIfNeeded(String date) {
		int fullStringSize = 6;
		if(date.length() != fullStringSize) {
			for(int a=date.length();a<fullStringSize;a++) {
				date = "0" + date;
			}
		}
		return date;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DateCreate create = new DateCreate();
		
		String dateStr = "81909";
		//String dateStr = "20090819";
		
		SimpleDateFormat formatter = new SimpleDateFormat("MMddyy");
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		try {
			dateStr = create.prependZeroIfNeeded(dateStr);
			Date date = formatter.parse(dateStr);
			System.out.println(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
	}

}
