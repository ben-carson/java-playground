package net.bencarson.playground;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHash {
	public static void main(String[] args) {
		Collection<String> newSet = new LinkedHashSet<String>();
		newSet.add("R");
		newSet.add("A");
		newSet.add("T");
		newSet.add("Dirty");
		
		Iterator<String> itr = newSet.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		
		for(String tempStr : newSet) {
			System.out.println(tempStr);
		}
	}
}