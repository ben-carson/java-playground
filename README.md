Java Playground
---
This is a simple collection of throwaway Java code for my personal use. I try out new things, build simple examples using a new technology I'm working with, and solidify my understanding of the Java language.<br>
Lots of bad code in here, I'm sure. But mostly, I'm just trying to learn a simple idea or technique, everything goes to the wayside.
